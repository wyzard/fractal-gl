// Copyright 2011 Michael B. Paul <mike@wyzardry.net>
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#include "ui/main-window.hh"

#include <cassert>
#include <sstream>

#include <gtkglmm.h>

char const MainWindow::ui_file_name[] = "main-window-ui.xml";

MainWindow::MainWindow() {
  init_uimanager();

  // Set basic window properties
  set_title("Fractal GL");
  set_default_size(800, 600);

  // Enable keyboard accelerators for menu/toolbar actions
  add_accel_group(m_rp_uimanager->get_accel_group());

  // Main vbox for overall menubar/content/statusbar layout
  Gtk::VBox *mp_main_vbox = manage(new Gtk::VBox());
  add(*mp_main_vbox);

  // Add main menubar
  Gtk::MenuBar *mp_main_menubar = dynamic_cast<Gtk::MenuBar*>(m_rp_uimanager->get_widget("/ui/MainMenubar"));
  if (mp_main_menubar) {
    mp_main_vbox->pack_start(*mp_main_menubar, Gtk::PACK_SHRINK);
    mp_main_menubar->show();
  }

  // Add terrain display
  m_mp_fractal_drawingarea = manage(new FractalDrawingArea());
  mp_main_vbox->pack_start(*m_mp_fractal_drawingarea, Gtk::PACK_EXPAND_WIDGET);
  m_mp_fractal_drawingarea->show();

  // Add statusbar
  m_mp_statusbar = manage(new Gtk::Statusbar());
  m_mp_statusbar->set_spacing(16);
  mp_main_vbox->pack_end(*m_mp_statusbar, Gtk::PACK_SHRINK);

  // Add framerate label to statusbar
  m_mp_framerate_label = manage(new Gtk::Label());
  m_mp_framerate_label->set_single_line_mode();
  m_mp_statusbar->pack_start(*m_mp_framerate_label, Gtk::PACK_SHRINK);
  m_mp_framerate_label->show();

  // Arrange to update the radio buttons in the menu when Julia mode is toggled
  m_mp_fractal_drawingarea->signal_julia_mode_changed_event().connect(sigc::mem_fun(*this, &MainWindow::on_fractal_drawingarea_julia_mode_changed));

  // Arrange for the framerate label to be updated each time it changes
  m_mp_fractal_drawingarea->signal_frame_time_changed_event().connect(sigc::mem_fun(*this, &MainWindow::on_fractal_drawingarea_frame_time_changed));

  m_mp_statusbar->show();

  mp_main_vbox->show();

  // Enable smooth shading by default
  m_rp_action_view_smoothshade->activate();

  m_rp_action_fractal_iteration_limit->set_current_value(m_mp_fractal_drawingarea->iteration_limit());
}

void MainWindow::init_uimanager() {
  // Create UI manager and insert action groups
  m_rp_actions_base = Gtk::ActionGroup::create("BaseActions");
  m_rp_actions_fractal = Gtk::ActionGroup::create("FractalActions");
  m_rp_uimanager = Gtk::UIManager::create();
  m_rp_uimanager->insert_action_group(m_rp_actions_base);
  m_rp_uimanager->insert_action_group(m_rp_actions_fractal);

  // Load static UI definition
  m_rp_uimanager->add_ui_from_file(ui_file_name);

  // Create base actions
  m_rp_actions_base->add(Gtk::Action::create("File", "_File"));
  m_rp_actions_base->add(Gtk::Action::create("File_Quit", Gtk::Stock::QUIT),
                         sigc::mem_fun(*this, &MainWindow::on_action_file_quit));
  m_rp_actions_base->add(Gtk::Action::create("View", "_View"));
  m_rp_action_view_smoothshade = Gtk::ToggleAction::create("View_SmoothShade", "_Smooth Shading");
  m_rp_action_view_smoothshade->signal_toggled().connect(sigc::mem_fun(*this, &MainWindow::on_action_view_smoothshade_toggled));
  m_rp_actions_base->add(m_rp_action_view_smoothshade);
  m_rp_actions_base->add(Gtk::Action::create("View_Reset", "_Reset View"),
                         sigc::mem_fun(*this, &MainWindow::on_action_view_reset));

  // Create the fractal-selection actions.
  Gtk::RadioAction::Group action_group_fractal;
  m_rp_action_fractal_mandelbrot = Gtk::RadioAction::create(action_group_fractal, "Fractal_Mandelbrot", "_Mandelbrot");
  m_rp_action_fractal_mandelbrot->signal_changed().connect(sigc::mem_fun(*this, &MainWindow::on_action_fractal_changed));
  m_rp_action_fractal_filled_julia = Gtk::RadioAction::create(action_group_fractal, "Fractal_FilledJulia", "_Julia");
  m_rp_actions_fractal->add(Gtk::Action::create("Fractal", "F_ractal"));
  m_rp_actions_fractal->add(m_rp_action_fractal_mandelbrot);
  m_rp_actions_fractal->add(m_rp_action_fractal_filled_julia);

  // Create the fractal iteration-count actions
  Gtk::RadioAction::Group action_group_fractal_iteration_limit;
  Gtk::UIManager::ui_merge_id fractal_iteration_limit_merge_id = m_rp_uimanager->new_merge_id();
  m_rp_actions_fractal->add(Gtk::Action::create("Fractal_IterationLimit", "_Iteration Limit"));
  for (int fractal_iteration_limit = FractalDrawingArea::ITERATION_LIMIT_MIN; fractal_iteration_limit <= FractalDrawingArea::ITERATION_LIMIT_MAX; fractal_iteration_limit *= 2) {
    // Build the name for the action
    std::stringstream name_builder;
    name_builder << "Fractal_IterationLimit_" << fractal_iteration_limit;
    std::string name = name_builder.str();

    // Build the label for the action
    std::stringstream label_builder;
    label_builder << fractal_iteration_limit;
    std::string label = label_builder.str();

    // Build the action itself
    Glib::RefPtr<Gtk::RadioAction> rp_action = Gtk::RadioAction::create(action_group_fractal_iteration_limit, name, label);
    m_rp_actions_fractal->add(rp_action);

    // Save one of the actions to represent the group wrt current_value and the "changed" signal
    if (!m_rp_action_fractal_iteration_limit) {
      m_rp_action_fractal_iteration_limit = rp_action;
    }

    // Store the iteration limit on the action so it can be retrieved in the change handler.
    rp_action->property_value().set_value(fractal_iteration_limit);

    // Add it to the UI
    m_rp_uimanager->add_ui(fractal_iteration_limit_merge_id, "/ui/MainMenubar/Fractal/Fractal_IterationLimit", name, name, Gtk::UI_MANAGER_AUTO, false);
  }
  m_rp_action_fractal_iteration_limit->signal_changed().connect(sigc::mem_fun(*this, &MainWindow::on_action_fractal_iteration_limit_changed));

  // Create widgets to realize UI
  m_rp_uimanager->ensure_update();
}

void MainWindow::on_action_file_quit() {
  hide();  // The main loop ends when this window is closed
}

void MainWindow::on_action_view_smoothshade_toggled() {
  bool const smooth_shade = m_rp_action_view_smoothshade->get_active();
  m_mp_fractal_drawingarea->smooth_shade(smooth_shade);
}

void MainWindow::on_action_view_reset() {
  m_mp_fractal_drawingarea->reset_view();
  m_mp_fractal_drawingarea->queue_draw();
}

void MainWindow::on_action_fractal_changed(Glib::RefPtr<Gtk::RadioAction> const &current) {
  if (current == m_rp_action_fractal_mandelbrot) {
    m_mp_fractal_drawingarea->julia_mode(false);
  }
  else if (current == m_rp_action_fractal_filled_julia) {
    m_mp_fractal_drawingarea->julia_mode(true);
  }
}

void MainWindow::on_action_fractal_iteration_limit_changed(Glib::RefPtr<Gtk::RadioAction> const &current) {
  m_mp_fractal_drawingarea->iteration_limit(current->get_current_value());
}

void MainWindow::on_fractal_drawingarea_julia_mode_changed(bool julia_mode) {
  if (julia_mode) {
    m_rp_action_fractal_filled_julia->activate();
  }
  else {
    m_rp_action_fractal_mandelbrot->activate();
  }
}

void MainWindow::on_fractal_drawingarea_frame_time_changed(unsigned int frame_time) {
  std::stringstream label_text;
  label_text << "FPS: " << (1.0e9 / frame_time);

  m_mp_framerate_label->set_text(label_text.str());
}
