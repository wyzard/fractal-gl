// Copyright 2011 Michael B. Paul <mike@wyzardry.net>
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#include "ui/fractal-drawingarea.hh"

#include <cmath>
#include <fstream>
#include <iostream>
#include <list>
#include <sstream>

#include <GL/glew.h>
#include <GL/gl.h>

namespace {
  // Rate of drag-zooming: dragging the full height of the window will
  // produce this much scaling.
  double const zoom_drag_rate = 4.0;

  // Rate of incremental mouse-wheel zooming, as a per-step scaling factor.
  double const zoom_wheel_step = std::pow(2.0, 0.25);  // 4 steps scales by 2.0

  char const view_vert_shader_source_path[] = "shaders/view.glslv";
  char const escape_time_frag_shader_source_path[] = "shaders/escape-time.glslf";
  char const mandelbrot_frag_shader_source_path[] = "shaders/mandelbrot.glslf";
  char const filled_julia_frag_shader_source_path[] = "shaders/filled-julia.glslf";

  // Vertex data for the triangle fan that's used to render the fractal.
  // The vertices here correspond to the four corners of the viewport.
  struct Vertex {
    float x, y;
  };
  Vertex vertex_data[] = {
    { 0.0, 0.0 },
    { 0.0, 1.0 },
    { 1.0, 1.0 },
    { 1.0, 0.0 },
  };

  // Color structure for texels
  struct Color {
    float r, g, b, a;

    Color(): r(0.0), g(0.0), b(0.0), a(0.0) { }
    Color(float r, float g, float b, float a): r(r), g(g), b(b), a(a) { }
  };

  // Static buffer used for loading a 1D texture into GL
  unsigned int const SHADING_TEXTURE_RESOLUTION = 4096;
  Color shading_texture[SHADING_TEXTURE_RESOLUTION];

  // Helper function for linear interpolation.
  double lerp(double a, double b, double f) {
    return a * (1.0 - f) + b * f;
  }

  // Helper function to define a 1D texture for shading.
  // (The texture is a procedurally-generated gradient, hard-coded for now.)
  void generate_shading_texture(Color const &a, Color const &b) {
    for (unsigned int i = 0; i < SHADING_TEXTURE_RESOLUTION; i += 1) {
      // Calculate this texel's texture coordinate.
      double const s = static_cast<double>(i) / static_cast<double>(SHADING_TEXTURE_RESOLUTION);

      // Calculate interpolation factor for a sinusoidal gradient.
      double const f = -std::cos(s * M_PI * 2.0) * 0.5 + 0.5;

      // Interpolate to define this texel's color.
      Color &texel = shading_texture[i];
      texel.r = lerp(a.r, b.r, f);
      texel.g = lerp(a.g, b.g, f);
      texel.b = lerp(a.b, b.b, f);
      texel.a = lerp(a.a, b.a, f);
    }
  }

  // Helper functions to compile and link shader programs.
  bool read_shader_source(char const *source_path, std::string &source) {
    std::ifstream source_stream(source_path);
    if (!source_stream.is_open()) return false;

    std::stringstream source_buf;
    source_buf << source_stream.rdbuf();
    source.assign(source_buf.str());
    return true;
  }
  bool compile_shader(GLenum type, std::string const &source, GLuint &shader, std::string &info_log) {
    shader = glCreateShader(type);

    // Load the shader's source code.
    char const *source_ptr = source.data();
    int const source_length = source.length();
    glShaderSource(shader, 1, &source_ptr, &source_length);

    glCompileShader(shader);

    // Check whether it succeeded.
    GLint compile_status;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compile_status);

    // Get the info log, containing any warnings/errors from the compile.
    GLint info_log_length;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_log_length);
    char info_log_data[info_log_length];
    glGetShaderInfoLog(shader, info_log_length, NULL, info_log_data);
    info_log.assign(info_log_data, info_log_length);

    return compile_status;
  }
  bool link_shader_program(std::list<GLuint> const &shaders, GLuint &program, std::string &info_log) {
    program = glCreateProgram();

    // Attach all the specified shaders to the program.
    for (std::list<GLuint>::const_iterator
         iter  = shaders.begin();
         iter != shaders.end();
         iter++) {
      GLuint shader = *iter;
      glAttachShader(program, shader);
    }

    glLinkProgram(program);

    // Check whether it succeeded.
    GLint link_status;
    glGetProgramiv(program, GL_LINK_STATUS, &link_status);

    // Get the info log, containing any warnings/errors from the link.
    GLint info_log_length;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &info_log_length);
    char info_log_data[info_log_length];
    glGetProgramInfoLog(program, info_log_length, NULL, info_log_data);
    info_log.assign(info_log_data, info_log_length);

    return link_status;
  }
}

FractalDrawingArea::FractalDrawingArea(): Gtk::GL::DrawingArea(Gdk::GL::Config::create(Gdk::GL::MODE_RGB | Gdk::GL::MODE_DOUBLE)) {
  add_events(Gdk::BUTTON_PRESS_MASK);
  add_events(Gdk::BUTTON_RELEASE_MASK);
  add_events(Gdk::BUTTON_MOTION_MASK);
  add_events(Gdk::POINTER_MOTION_HINT_MASK);
  drag_state.mode = DRAG_NONE;

  m_iteration_limit = ITERATION_LIMIT_MIN;
}

void FractalDrawingArea::reset_view() {
  // Initial default region of the fractal to render.
  m_viewport_controller.reset();
  m_viewport_controller.aspect(get_width(), get_height());
  m_viewport_controller.pan(-0.5, 0.0);
  m_viewport_controller.zoom(0.5);
}

void FractalDrawingArea::on_realize() {
  Gtk::GL::DrawingArea::on_realize();

  reset_view();

  // Now that the widget is realized, it has a GL context.
  Glib::RefPtr<Gdk::GL::Drawable> rp_gl_drawable = get_gl_drawable();
  Glib::RefPtr<Gdk::GL::Context>  rp_gl_context  = get_gl_context();
  if (rp_gl_drawable->gl_begin(rp_gl_context)) {
    // Initialize GLEW.  Note that if there are multiple instances of this
    // widget, this results in GLEW being initialized multiple times (which is
    // harmless by itself) and subsequently everyone uses the results from the
    // one that was realized most recently.  The assumption is that the
    // pointers and flags established by glewInit() are the same across all GL
    // contexts in the same process, so that it's OK to call glewInit() with
    // one context activated and then use the results in a different context.
    if (glewInit() != GLEW_OK) {
      std::cerr << "glewInit() failed!" << std::endl;
    }

    // Check OpenGL version.
    // We require at least 2.0 so we don't have to treat essentials like
    // fragment shaders as optional extensions that might be unavailable.
    if (!GLEW_VERSION_2_0) {
      std::cerr << "OpenGL 2.0 not supported!" << std::endl;
    }

    // Create a query object for measuring frame rendering time, if supported.
    if (GLEW_EXT_timer_query) {
      glGenQueries(1, &m_timer_query_id);
    }
    else {  // Not supported
      m_timer_query_id = 0;
    }

    // Define the texture used for shading.
    generate_shading_texture(Color(0.1, 0.2, 0.3, 1.0), Color(1.0, 1.0, 1.0, 1.0));
    glGenTextures(1, &m_shading_texture);
    glBindTexture(GL_TEXTURE_1D, m_shading_texture);
    glTexParameteri(GL_TEXTURE_1D, GL_GENERATE_MIPMAP, GL_TRUE);
    glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA, SHADING_TEXTURE_RESOLUTION, 0, GL_RGBA, GL_FLOAT, shading_texture);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_REPEAT);

    // Compile view vertex shader.
    {
      std::string source, info_log;

      if (!read_shader_source(view_vert_shader_source_path, source)) {
        std::cerr << "Failed to load shader code: " << view_vert_shader_source_path << std::endl;
        m_programs.vert_shaders.view = 0;
      }

      if (!compile_shader(GL_VERTEX_SHADER, source, m_programs.vert_shaders.view, info_log)) {
        std::cerr << "Failed to compile view vertex shader:\n" << info_log << std::endl;
      }
    }

    // Compile escape-time fractal shader.
    {
      std::string source, info_log;

      if (!read_shader_source(escape_time_frag_shader_source_path, source)) {
        std::cerr << "Failed to load shader code: " << escape_time_frag_shader_source_path << std::endl;
        m_programs.frag_shaders.escape_time = 0;
      }

      if (!compile_shader(GL_FRAGMENT_SHADER, source, m_programs.frag_shaders.escape_time, info_log)) {
        std::cerr << "Failed to compile escape-time fragment shader:\n" << info_log << std::endl;
      }
    }

    // Compile Mandelbrot fragment shader.
    {
      std::string source, info_log;

      if (!read_shader_source(mandelbrot_frag_shader_source_path, source)) {
        std::cerr << "Failed to load shader code: " << mandelbrot_frag_shader_source_path << std::endl;
        m_programs.frag_shaders.mandelbrot = 0;
      }

      if (!compile_shader(GL_FRAGMENT_SHADER, source, m_programs.frag_shaders.mandelbrot, info_log)) {
        std::cerr << "Failed to compile Mandelbrot fragment shader:\n" << info_log << std::endl;
      }
    }

    // Compile Julia fragment shader.
    {
      std::string source, info_log;

      if (!read_shader_source(filled_julia_frag_shader_source_path, source)) {
        std::cerr << "Failed to load shader code: " << filled_julia_frag_shader_source_path << std::endl;
        m_programs.frag_shaders.filled_julia = 0;
      }

      if (!compile_shader(GL_FRAGMENT_SHADER, source, m_programs.frag_shaders.filled_julia, info_log)) {
        std::cerr << "Failed to compile Julia fragment shader:\n" << info_log << std::endl;
      }
    }

    // Link the Mandelbrot rendering program.
    {
      std::list<GLuint> shaders;
      shaders.push_back(m_programs.vert_shaders.view);
      shaders.push_back(m_programs.frag_shaders.escape_time);
      shaders.push_back(m_programs.frag_shaders.mandelbrot);

      std::string info_log;

      if (!link_shader_program(shaders, m_programs.mandelbrot, info_log)) {
        std::cerr << "Failed to link Mandelbrot shader program:\n" << info_log << std::endl;
      }
    }

    // Link the Julia rendering program.
    {
      std::list<GLuint> shaders;
      shaders.push_back(m_programs.vert_shaders.view);
      shaders.push_back(m_programs.frag_shaders.escape_time);
      shaders.push_back(m_programs.frag_shaders.filled_julia);

      std::string info_log;

      if (!link_shader_program(shaders, m_programs.filled_julia, info_log)) {
        std::cerr << "Failed to link Julia shader program:\n" << info_log << std::endl;
      }
    }

    // Look up locations of uniforms and attributes in the Mandelbrot program.
    {
      m_program_vars.mandelbrot.domain_min = glGetUniformLocation(m_programs.mandelbrot, "domain_min");
      m_program_vars.mandelbrot.domain_max = glGetUniformLocation(m_programs.mandelbrot, "domain_max");

      m_program_vars.mandelbrot.iteration_limit = glGetUniformLocation(m_programs.mandelbrot, "iter_limit");
      m_program_vars.mandelbrot.smooth_shade = glGetUniformLocation(m_programs.mandelbrot, "smooth_shade");
      m_program_vars.mandelbrot.shading_density = glGetUniformLocation(m_programs.mandelbrot, "shading_density");
      m_program_vars.mandelbrot.shading_texture = glGetUniformLocation(m_programs.mandelbrot, "shading_texture");

      m_program_vars.mandelbrot.viewport_pos = glGetAttribLocation(m_programs.mandelbrot, "viewport_pos");
    }

    // Look up locations of uniforms and attributes in the Julia program.
    {
      m_program_vars.filled_julia.domain_min = glGetUniformLocation(m_programs.filled_julia, "domain_min");
      m_program_vars.filled_julia.domain_max = glGetUniformLocation(m_programs.filled_julia, "domain_max");

      m_program_vars.filled_julia.iteration_limit = glGetUniformLocation(m_programs.filled_julia, "iter_limit");
      m_program_vars.filled_julia.smooth_shade = glGetUniformLocation(m_programs.filled_julia, "smooth_shade");
      m_program_vars.filled_julia.shading_density = glGetUniformLocation(m_programs.filled_julia, "shading_density");
      m_program_vars.filled_julia.shading_texture = glGetUniformLocation(m_programs.filled_julia, "shading_texture");

      m_program_vars.filled_julia.julia_c = glGetUniformLocation(m_programs.filled_julia, "c");

      m_program_vars.filled_julia.viewport_pos = glGetAttribLocation(m_programs.filled_julia, "viewport_pos");
    }

    // Create a vertex buffer for the triangle fan that will render the fractal.
    {
      glGenBuffers(1, &m_vertex_buffer);
      glBindBuffer(GL_ARRAY_BUFFER, m_vertex_buffer);
      glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_data), vertex_data, GL_STATIC_DRAW);
    }

    rp_gl_drawable->gl_end();
  }
}

bool FractalDrawingArea::on_button_press_event(GdkEventButton *event) {
  bool result = Gtk::GL::DrawingArea::on_button_press_event(event);

  // Only start a drag if we're not in one already.
  if (drag_state.mode == DRAG_NONE) {
    switch (event->button) {
    case 1:  // Left
      m_transient_julia_mode = false;  // Left-click makes it stick
      drag_state.mode = DRAG_PAN;
      break;

    case 2:  // Middle
      drag_state.mode = DRAG_ZOOM;
      break;

    case 3:  // Right
      if (!m_julia_mode) {
        julia_mode(true);
        m_transient_julia_mode = true;
        update_julia_c(event->x, event->y);
        queue_draw();
      }
      break;
    }

    drag_state.prev_x = event->x;
    drag_state.prev_y = event->y;
  }

  return result;
}

bool FractalDrawingArea::on_button_release_event(GdkEventButton *event) {
  bool result = Gtk::GL::DrawingArea::on_button_release_event(event);

  if ((drag_state.mode == DRAG_PAN) && (event->button == 1)) {
    drag_state.mode = DRAG_NONE;
  }
  else if ((drag_state.mode == DRAG_ZOOM) && (event->button == 2)) {
    drag_state.mode = DRAG_NONE;
  }
  else if (m_julia_mode && m_transient_julia_mode && (event->button == 3)) {
    julia_mode(false);
    queue_draw();
  }

  return result;
}

bool FractalDrawingArea::on_scroll_event(GdkEventScroll *event) {
  bool result = Gtk::GL::DrawingArea::on_scroll_event(event);

  // Get the current mouse position in viewport coordinates.
  double const xf =        event->x / get_width();
  double const yf = 1.0 - (event->y / get_height());  // Pixel coords increase downward

  // Zoom toward or away from the mouse position.
  if (event->direction == GDK_SCROLL_UP) {
    m_viewport_controller.zoom(zoom_wheel_step, xf, yf);
  }
  else if (event->direction == GDK_SCROLL_DOWN) {
    m_viewport_controller.zoom(1.0 / zoom_wheel_step, xf, yf);
  }

  queue_draw();

  return result;
}

bool FractalDrawingArea::on_motion_notify_event(GdkEventMotion *event) {
  bool result = Gtk::GL::DrawingArea::on_motion_notify_event(event);

  double const dx = event->x - drag_state.prev_x;
  double const dy = event->y - drag_state.prev_y;

  double const xf =  dx / get_width();
  double const yf = -dy / get_height();  // Pixel coords increase downward

  switch (drag_state.mode) {
  case DRAG_PAN:
    // Move the viewport in the opposite direction of the drag, so that points
    // in the view follow the mouse.
    m_viewport_controller.pan(-xf, -yf);
    queue_draw();
    break;

  case DRAG_ZOOM:
    // Dragging is linear but zooming is exponential, so exponentiate the drag
    // distance.  This ensures that the same total drag distance always
    // produces the same total zoom, regardless of whether it's done in many
    // small steps or a few large ones.
    m_viewport_controller.zoom(std::pow(zoom_drag_rate, yf));
    queue_draw();
    break;

  case DRAG_NONE:
    // Nothing to do.
    break;
  }

  if (m_julia_mode && m_transient_julia_mode) {
    update_julia_c(event->x, event->y);
    queue_draw();
  }

  drag_state.prev_x = event->x;
  drag_state.prev_y = event->y;

  return result;
}

bool FractalDrawingArea::on_configure_event(GdkEventConfigure *event) {
  bool result = Gtk::GL::DrawingArea::on_configure_event(event);

  m_viewport_controller.aspect(get_width(), get_height());

  Glib::RefPtr<Gdk::GL::Drawable> rp_gl_drawable = get_gl_drawable();
  Glib::RefPtr<Gdk::GL::Context>  rp_gl_context  = get_gl_context();
  if (rp_gl_drawable->gl_begin(rp_gl_context)) {
    glViewport(0, 0, get_width(), get_height());

    rp_gl_drawable->gl_end();
  }

  return result;
}

bool FractalDrawingArea::on_expose_event(GdkEventExpose *event) {
  bool result = Gtk::GL::DrawingArea::on_expose_event(event);

  Glib::RefPtr<Gdk::GL::Drawable> rp_gl_drawable = get_gl_drawable();
  Glib::RefPtr<Gdk::GL::Context>  rp_gl_context  = get_gl_context();
  if (rp_gl_drawable->gl_begin(rp_gl_context)) {
    draw();  // Redraw the fractal

    rp_gl_drawable->swap_buffers();
    rp_gl_drawable->gl_end();
  }

  return result;
}

void FractalDrawingArea::update_julia_c(double x, double y) {
  double const xf =        x / get_width();
  double const yf = 1.0 - (y / get_height());  // Pixel coords increase downward

  m_julia_c.x = m_viewport_controller.mapX(xf);
  m_julia_c.y = m_viewport_controller.mapY(yf);
}

void FractalDrawingArea::draw() {
  if (GLEW_EXT_timer_query && m_timer_query_id) {
    // Start timing this frame
    glBeginQuery(GL_TIME_ELAPSED_EXT, m_timer_query_id);
  }

  glClear(GL_COLOR_BUFFER_BIT);

  glUseProgram(m_julia_mode ? m_programs.filled_julia : m_programs.mandelbrot);

  // Bind the texture used for shading the fractal.
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_1D, m_shading_texture);

  // Use mipmaps only when smooth-shading, since GL's LOD calculation assumes
  // smooth continuous texture coordinates, not discrete ones.
  if (m_smooth_shade) {
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  }
  else {
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  }

  // Configure the shaders.
  ProgramVars const &program_vars = m_julia_mode ? m_program_vars.filled_julia : m_program_vars.mandelbrot;
  glUniform2f(program_vars.domain_min,
              m_viewport_controller.mapX(0),
              m_viewport_controller.mapY(0));
  glUniform2f(program_vars.domain_max,
              m_viewport_controller.mapX(1),
              m_viewport_controller.mapY(1));
  glUniform1i(program_vars.iteration_limit, m_iteration_limit);
  glUniform1i(program_vars.smooth_shade, m_smooth_shade);
  glUniform1f(program_vars.shading_density, 32.0);  // Not configurable for now
  glUniform1i(program_vars.shading_texture, 0);  // Use texture bound to GL_TEXTURE0

  if (m_julia_mode) {
    glUniform2f(program_vars.julia_c, m_julia_c.x, m_julia_c.y);
  }

  // Render the fractal.
  glBindBuffer(GL_ARRAY_BUFFER, m_vertex_buffer);
  glEnableVertexAttribArray(program_vars.viewport_pos);
  glVertexAttribPointer(program_vars.viewport_pos, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), offsetof(Vertex, x));
  glDrawArrays(GL_TRIANGLE_FAN, 0, sizeof(vertex_data) / sizeof(vertex_data[0]));

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glUseProgram(0);

  if (GLEW_EXT_timer_query && m_timer_query_id) {
    // Stop the frame timer
    glEndQuery(GL_TIME_ELAPSED_EXT);

    // Read the timer (waits for all timed commands to finish executing)
    GLuint timer_query_result = 0;
    glGetQueryObjectuiv(m_timer_query_id, GL_QUERY_RESULT, &timer_query_result);

    m_signal_frame_time_changed_event.emit(timer_query_result);
  }

  for (GLenum error = glGetError(); error != GL_NO_ERROR; error = glGetError()) {
    std::cerr << "GL error: " << gluErrorString(error) << std::endl;
  }
}
