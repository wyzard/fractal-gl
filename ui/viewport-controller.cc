// Copyright 2011 Michael B. Paul <mike@wyzardry.net>
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#include "ui/viewport-controller.hh"

ViewportController::ViewportController() {
  reset();
}

void ViewportController::pan(double x, double y) {
  m_center_x += x * m_height * m_aspect;
  m_center_y += y * m_height;
}

void ViewportController::zoom(double factor, double x, double y) {
  // Find the fixed point's original position relative to the viewport center.
  double const before_x = x - 0.5;
  double const before_y = y - 0.5;

  // Scale the viewport.  This moves points toward or away from the center,
  // changing their viewport coordinates.
  m_height /= factor;

  // Find the fixed point's new position relative to the viewport center.
  double const after_x = before_x * factor;
  double const after_y = before_y * factor;

  // Pan to put the fixed point back at its old viewport coordinate.
  pan(after_x - before_x, after_y - before_y);
}

void ViewportController::aspect(double aspect) {
  m_aspect = aspect;
}

void ViewportController::reset() {
  m_center_x = 0;
  m_center_y = 0;
  m_height = 1;
  m_aspect = 1;
}

float ViewportController::mapX(double x) const {
  return m_center_x + (x - 0.5) * m_height * m_aspect;
}

float ViewportController::mapY(double y) const {
  return m_center_y + (y - 0.5) * m_height;
}
