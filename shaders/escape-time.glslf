// Copyright 2011 Michael B. Paul <mike@wyzardry.net>
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#version 110

// Helper functions for computing escape-time fractals.

// Bailout value: iteration terminates when the magnitude of the value exceeds
// this threshold.  For correctness, must be a distance at which all points are
// known to not be in the set; 2 is a common minimum value.  Large values
// produce better shading, but cannot be too large:  escaping points must exceed
// the *square* of this value (due to Pythagoras) before they overflow the
// floating-point exponent limit.
const float bailout = 1048576.0;
const float bailout_sq = bailout * bailout;

// If smooth shading is enabled, the "normalized iteration count" algorithm is
// used to adjust the iteration count when bailout occurs.
uniform bool smooth_shade;

// Scaling factor for translating a fragment's iteration count into a texture
// coordinate for scaling; this determines how closely-spaced the "bands" of
// color are.
uniform float shading_density;

// 1D Texture sampler used for assigning colors to iteration values.
uniform sampler1D shading_texture;

// Checks whether a point has exceeded the bailout value.
// The point's magnitude is given in squared form to avoid making the caller
// take a square root only to square it again here.
bool check_bailout(float length_sq) {
  return length_sq > bailout_sq;
}

// Computes a shading color for a pixel that has reached bailout.
// Arguments are the squared magnitude that was given to check_bailout(),
// and the iteration number when bailout occurred.
vec4 shade_bailout(float length_sq, int iter) {
  float n = float(iter);

  if (smooth_shade) {
    // Apply "normalized iteration count" algorithm.
    // Different points that bail out in the same number of iterations are
    // distinguished by how far they went past the bailout value on the final
    // iteration.  The number subtracted here is in the [0..1) range, converting
    // each band of shading into a gradation that blends with the adjacent ones.
    float length = sqrt(length_sq);
    n -= log2(log(length) / log(bailout));
  }

  return texture1D(shading_texture, n / shading_density);
}
