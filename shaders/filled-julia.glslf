// Copyright 2011 Michael B. Paul <mike@wyzardry.net>
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#version 110

// Declare functions used from other modules.
bool check_bailout(float length_sq);
vec4 shade_bailout(float length_sq, int iter);

// The color used for fragments that are in the set.
// (Fragments outide the set are passed to shade_bailout().)
const vec4 in_set_color = vec4(0.0, 0.0, 0.0, 0.0); 

// Number of iterations after which a point is considered to be in the set if
// it hasn't reached bailout yet
uniform int iter_limit;

// Point within the domain whose Julia set is to be rendered
uniform vec2 c;

// Interpolated domain coordinates
varying vec2 domain_pos;

// Computes the square of a complex number.
// (a+bi)^2 = (a^2 - b^2) + (2ab)i
vec2 complex_square(in vec2 p) {
  float re = (p.x * p.x) - (p.y * p.y);
  float im = 2.0 * p.x * p.y;
  return vec2(re, im);
}

vec4 filled_julia(vec2 p) {
  float length_sq;
  int iter;
  bool bailout = false;
  
  for (iter = 0; iter < iter_limit; iter += 1) {
    // Compute one Julia iteration.
    p = complex_square(p) + c;

    // Check for bailout
    length_sq = dot(p, p);
    if (check_bailout(length_sq)) {
      bailout = true;
      break;
    }
  }

  // Artificially increase the iteration count for fragments that didn't bail
  // out, so that partial deriviatives at the edge of the set will reflect the
  // fact that the "true" iteration count is asymptotic to infinity there.
  if (!bailout) {
    iter += iter_limit;
  }

  // Calculate shading based on iteration count.  This is done even for
  // fragments within the set, so that GL can compute texture LOD for fragments
  // just outside the set, adjacent to fragments within.
  vec4 shade = shade_bailout(length_sq, iter);

  if (bailout) {
    // The point is not in the set, so use the iteration-based shading.
    return shade;
  }
  else {
    // Bailout not reached before the iteration limit,
    // so the point is probably in the set.
    return in_set_color;
  }
}

void main() {
  gl_FragColor = filled_julia(domain_pos);
}
