// Copyright 2011 Michael B. Paul <mike@wyzardry.net>
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#version 110

// Minimum and maximum bounds in the domain of the fractal function.
uniform vec2 domain_min, domain_max;

// Viewport coordinates of this vertex, in [0..1] range.
attribute vec2 viewport_pos;

// Interpolated domain coordinates
varying vec2 domain_pos;

void main() {
  // Map viewport coordinates into OpenGL clip coordinates.
  // This makes the [0..1] viewport rectangle fill the whole window.
  gl_Position.xy = viewport_pos * 2.0 - 1.0;
  gl_Position.zw = vec2(0.0, 1.0);

  // Map viewort coordinates into domain coordinates for the fragment shader.
  // This determines which region of the fractal will be rendered.
  domain_pos = mix(domain_min, domain_max, viewport_pos);
}
