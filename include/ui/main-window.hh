// Copyright 2011 Michael B. Paul <mike@wyzardry.net>
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#ifndef MAIN_WINDOW_HH
#define MAIN_WINDOW_HH

#include "ui/fractal-drawingarea.hh"

#include <gtkmm.h>

class MainWindow : public Gtk::Window {
public:
  MainWindow();

  static char const ui_file_name[];

private:
  // The widget that draws the fractal
  FractalDrawingArea *m_mp_fractal_drawingarea;

  // The status bar
  Gtk::Statusbar *m_mp_statusbar;

  // Labels in the status bar
  Gtk::Label *m_mp_framerate_label;

  // Management of menus & toolbars
  void init_uimanager();
  Glib::RefPtr<Gtk::ActionGroup> m_rp_actions_base;
  Glib::RefPtr<Gtk::ActionGroup> m_rp_actions_fractal;
  Glib::RefPtr<Gtk::UIManager> m_rp_uimanager;

  // Actions with state that needs to be manipulated
  Glib::RefPtr<Gtk::ToggleAction> m_rp_action_view_smoothshade;
  Glib::RefPtr<Gtk::RadioAction> m_rp_action_fractal_mandelbrot;
  Glib::RefPtr<Gtk::RadioAction> m_rp_action_fractal_filled_julia;
  Glib::RefPtr<Gtk::RadioAction> m_rp_action_fractal_iteration_limit;

  // Signal handlers
  void on_action_file_quit();
  void on_action_view_smoothshade_toggled();
  void on_action_view_reset();
  void on_action_fractal_changed(Glib::RefPtr<Gtk::RadioAction> const &current);
  void on_action_fractal_iteration_limit_changed(Glib::RefPtr<Gtk::RadioAction> const &current);
  void on_fractal_drawingarea_julia_mode_changed(bool julia_mode);
  void on_fractal_drawingarea_frame_time_changed(unsigned int frame_time);
};

#endif  // ndef MAIN_WINDOW_HH
