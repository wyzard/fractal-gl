// Copyright 2011 Michael B. Paul <mike@wyzardry.net>
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#ifndef FRACTAL_VIEWPORT_CONTROLLER_HH
#define FRACTAL_VIEWPORT_CONTROLLER_HH

// This class implements a "viewport" encompassing a rectangular region of a
// Cartesian plane.
//
// Locations within the viewport rectangle are identified by "viewport
// coordinates" whose X and Y components range from 0 to 1.
//
// The plane being viewed is referred to as the "domain", in the mathematical
// sense (the set of valid inputs to a function), and coordinates on the plane
// are referred to as "domain coordinates".
//
// Functions are provided to move the viewport around the domain, and to
// translate viewport coordinates into domain coordinates.
class ViewportController {
public:
  // Constructs a ViewportController.
  // The initial viewport is configured as if reset() had been called.
  ViewportController();

  // Adjusts the viewport's position.
  // The offset is specified in viewport coordinates, not domain coordinates.
  void pan(double x, double y);

  // Zooms the viewport by a factor, using its center as a fixed point.
  // Numbers greater than 1 magnify the view; numbers less than 1 shrink it.
  void zoom(double factor);

  // Zooms the viewport by a factor, using a specified fixed point.
  // The fixed point is a point (expressed in viewport coordinates) whose
  // position will not change; all other points in the view will move toward or
  // away from it.
  void zoom(double factor, double x, double y);

  // Adjusts the viewport's aspect ratio.
  // The viewport's height is equal to its scale; its width is the product of
  // its scale and its aspect ratio.
  void aspect(double width, double height);
  void aspect(double aspect);

  // Resets the viewport to center on the domain coordinate (0, 0) with a
  // scale of 1 and a square aspect ratio.
  void reset();

  // Maps viewport coordinates into domain coordinates.
  float mapX(double x) const;
  float mapY(double y) const;

private:
  double m_center_x;
  double m_center_y;
  double m_height;
  double m_aspect;
};

inline void ViewportController::aspect(double width, double height) {
  aspect(width / height);
}

inline void ViewportController::zoom(double factor) {
  return zoom(factor, 0.5, 0.5);
}

#endif  // ndef FRACTAL_VIEW_PORT_CONTROLLER_HH
