// Copyright 2011 Michael B. Paul <mike@wyzardry.net>
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#ifndef FRACTAL_DRAWINGAREA_HH
#define FRACTAL_DRAWINGAREA_HH

#include "ui/viewport-controller.hh"

#include <string>

#include <GL/glew.h>

#include <gtkglmm.h>

class FractalDrawingArea : public Gtk::GL::DrawingArea {
public:
  FractalDrawingArea();

  bool julia_mode() const { return m_julia_mode; }
  void julia_mode(bool flag) {
    m_julia_mode = flag;
    m_transient_julia_mode = false;
    m_signal_julia_mode_changed_event.emit(m_julia_mode);
    queue_draw();
  }

  void reset_view();

  static int const ITERATION_LIMIT_MIN = 128;
  static int const ITERATION_LIMIT_MAX = 1024;
  int iteration_limit() const { return m_iteration_limit; }
  void iteration_limit(int value) {
    m_iteration_limit = value;
    queue_draw();
  }
  
  bool smooth_shade() const { return m_smooth_shade; }
  void smooth_shade(bool const flag) {
    m_smooth_shade = flag;
    queue_draw();
  }

  sigc::signal<void, bool> &signal_julia_mode_changed_event() {
    return m_signal_julia_mode_changed_event;
  }

  sigc::signal<void, unsigned int> &signal_frame_time_changed_event() {
    return m_signal_frame_time_changed_event;
  }

protected:

  // Overrides
  void on_realize();
  bool on_button_press_event(GdkEventButton *event);
  bool on_button_release_event(GdkEventButton *event);
  bool on_scroll_event(GdkEventScroll *event);
  bool on_motion_notify_event(GdkEventMotion *event);
  bool on_configure_event(GdkEventConfigure *event);
  bool on_expose_event(GdkEventExpose *event);

private:
  void draw();

  // Signal handlers

  sigc::signal<void, bool> m_signal_julia_mode_changed_event;
  sigc::signal<void, unsigned int> m_signal_frame_time_changed_event;

  ViewportController m_viewport_controller;

  enum DragMode { DRAG_NONE, DRAG_PAN, DRAG_ZOOM };
  struct {
    DragMode mode;
    float prev_x, prev_y;
  } drag_state;

  bool m_julia_mode, m_transient_julia_mode;
  struct {
    double x, y;
  } m_julia_c;
  void update_julia_c(double x, double y);

  int m_iteration_limit;

  bool m_smooth_shade;

  GLuint m_timer_query_id;

  struct {
    GLuint mandelbrot;
    GLuint filled_julia;

    struct {
      GLuint view;
    } vert_shaders;

    struct {
      GLuint escape_time;
      GLuint mandelbrot;
      GLuint filled_julia;
    } frag_shaders;
  } m_programs;

  struct ProgramVars {
    // Uniforms defining the region of the domain to be rendered
    GLint domain_min;
    GLint domain_max;

    // Uniforms controlling rendering
    GLint iteration_limit;
    GLint smooth_shade;
    GLint shading_density;
    GLint shading_texture;

    // Uniform defining which Julia set is to be rendered;
    // not applicable to the Mandelbrot set.
    GLint julia_c;

    // Attribute defining the viewport coordinates of a vertex
    GLint viewport_pos;
  };
  struct {
    ProgramVars mandelbrot;
    ProgramVars filled_julia;
  } m_program_vars;

  GLuint m_vertex_buffer;

  GLuint m_shading_texture;
};

#endif  // ndef TERRAIN_DRAWINGAREA_HH
